export class User{
	constructor(
		
		public _id: number,
		public name: string,
		public nameEmpresa: string,
		public email: string,
		public telefono: string,
		public mensaje: string,
		public category: string,

		){}

}